export function Type() {
    return () => {
    };
}

export function Transform() {
    return () => {
    };
}

export function ArrayMaxSizeI18n() {
    return () => {
    };
}

export function ArrayMinSizeI18n() {
    return () => {
    };
}

export function IsArrayI18n() {
    return () => {
    };
}

export function IsBooleanI18n() {
    return () => {
    };
}

export function IsDateStringI18n() {
    return () => {
    };
}

export function IsEmailI18n() {
    return () => {
    };
}

export function IsEqualI18n() {
    return () => {
    };
}

export function IsInI18n() {
    return () => {
    };
}

export function IsIntI18n() {
    return () => {
    };
}

export function IsNumberI18n() {
    return () => {
    };
}

export function IsSameI18n() {
    return () => {
    };
}

export function IsStringI18n() {
    return () => {
    };
}

export function IsUuidI18n() {
    return () => {
    };
}

export function LengthI18n() {
    return () => {
    };
}

export function MaxI18n() {
    return () => {
    };
}

export function MinI18n() {
    return () => {
    };
}

export function MinLengthI18n() {
    return () => {
    };
}

export function MaxLengthI18n() {
    return () => {
    };
}

export function IsNotEmptyI18n() {
    return () => {
    };
}

export function ValidateNested() {
    return () => {
    };
}

export function IsOptional() {
    return () => {
    };
}

export function IsRequired() {
    return () => {
    };
}

export function Allow() {
    return () => {
    };
}

export function ValidateIf() {
    return () => {
    };
}

export function IsSame() {
    return () => {
    };
}

export function IsObject() {
    return () => {
    };
}