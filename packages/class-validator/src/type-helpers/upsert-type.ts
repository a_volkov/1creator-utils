import { ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { IsArrayI18n } from '../decorators/is-array.decorator';

interface IType<T = any> extends Function {
    new(...args: any[]): T;
}

export function UpsertType<T>(
    classRef: IType<T>,
): IType<{
    removeIds: any[],
    items: T[],
}> {
    abstract class UpsertTypeClass {
        @IsArrayI18n()
        removeIds: any[];

        @IsArrayI18n()
        @ValidateNested({ each: true })
        @Type(() => classRef)
        items: T[];
    }

    return UpsertTypeClass as IType<{
        removeIds: any[],
        items: T[],
    }>;
}