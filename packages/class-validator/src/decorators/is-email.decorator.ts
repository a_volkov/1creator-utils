import { IsEmail, ValidationOptions } from 'class-validator';
import ValidatorJS from 'validator';

export const IsEmailI18n = (
    opts?: ValidatorJS.IsEmailOptions,
    validationOptions?: ValidationOptions,
) => {
    return (target: object, key: string) => {
        IsEmail(opts, {
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен быть корректным адресом электронной почты'
                : validationOptions?.message ?? 'Неверный формат электронной почты',
        })(target, key);
    };
};
