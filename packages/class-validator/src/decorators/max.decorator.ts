import { Max, ValidationOptions } from 'class-validator';

export const MaxI18n = (max: number, validationOptions?: ValidationOptions) => {
    return (target: object, key: string) => {
        Max(max, {
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен быть меньше, чем $constraint1'
                : validationOptions?.message ??
          'Поле должно быть меньше, чем $constraint1',
        })(target, key);
    };
};
