import { IsArray, ValidationOptions } from 'class-validator';

export const IsArrayI18n = (validationOptions?: ValidationOptions) => {
    return (target: object, key: string) => {
        IsArray({
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен быть массивом'
                : validationOptions?.message ?? 'Поле должно быть массивом',
        })(target, key);
    };
};
