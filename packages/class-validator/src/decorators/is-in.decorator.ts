import { IsIn, ValidationOptions } from 'class-validator';

export const IsInI18n = (
    values: any[],
    validationOptions?: ValidationOptions,
) => {
    return (target: object, key: string) => {
        IsIn(values, {
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен равняться одному из следующих значений: ' +
          values.join(', ')
                : validationOptions?.message ??
          'Поле должно равняться одному из следующих значений: ' +
            values.join(', '),
        })(target, key);
    };
};
