import { MinLength, ValidationOptions } from 'class-validator';

export const MinLengthI18n = (
    min: number,
    validationOptions?: ValidationOptions,
) => {
    return (target: object, key: string) => {
        MinLength(min, {
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен содержать от $constraint1 символов'
                : validationOptions?.message ??
        'Поле должно содержать от $constraint1 символов',
        })(target, key);
    };
};
