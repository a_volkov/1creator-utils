import { ArrayMinSize, ValidationOptions } from 'class-validator';

export const ArrayMinSizeI18n = (
    min: number,
    validationOptions?: ValidationOptions,
) => {
    return (target: object, key: string) => {
        ArrayMinSize(min, {
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен содержать не менее $constraint1 значений'
                : validationOptions?.message ??
          'Поле должно содержать не менее $constraint1 значений',
        })(target, key);
    };
};
