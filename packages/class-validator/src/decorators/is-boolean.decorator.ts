import { IsBoolean, ValidationOptions } from 'class-validator';

export const IsBooleanI18n = (validationOptions?: ValidationOptions) => {
    return (target: object, key: string) => {
        IsBoolean({
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен быть булевым значением'
                : validationOptions?.message ??
          'Поле должно быть булевым значением',
        })(target, key);
    };
};
