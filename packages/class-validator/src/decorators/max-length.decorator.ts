import { MaxLength, ValidationOptions } from 'class-validator';

export const MaxLengthI18n = (
    max: number,
    validationOptions?: ValidationOptions,
) => {
    return (target: object, key: string) => {
        MaxLength(max, {
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен содержать до $constraint1 символов'
                : validationOptions?.message ??
        'Поле должно содержать до $constraint1 символов',
        })(target, key);
    };
};
