import { IsInt, ValidationOptions } from 'class-validator';

export const IsIntI18n = (validationOptions?: ValidationOptions) => {
    return (target: object, key: string) => {
        IsInt({
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен быть целочисленным значением'
                : validationOptions?.message ??
          'Поле должно быть целочисленным значением',
        })(target, key);
    };
};
