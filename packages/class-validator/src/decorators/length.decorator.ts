import { Length, ValidationOptions } from 'class-validator';

export const LengthI18n = (
    min: number,
    max: number,
    validationOptions?: ValidationOptions,
) => {
    return (target: object, key: string) => {
        Length(min, max, {
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен содержать от $constraint1 до $constraint2 символов'
                : validationOptions?.message ??
          'Поле должно содержать от $constraint1 до $constraint2 символов',
        })(target, key);
    };
};
