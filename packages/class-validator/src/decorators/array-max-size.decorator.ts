import { ArrayMaxSize, ValidationOptions } from 'class-validator';

export const ArrayMaxSizeI18n = (
    min: number,
    validationOptions?: ValidationOptions,
) => {
    return (target: object, key: string) => {
        ArrayMaxSize(min, {
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен содержать не более $constraint1 значений'
                : validationOptions?.message ??
          'Поле должно содержать не более $constraint1 значений',
        })(target, key);
    };
};
