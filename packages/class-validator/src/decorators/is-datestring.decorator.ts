import { IsDateString, ValidationOptions } from 'class-validator';

export const IsDateStringI18n = (validationOptions?: ValidationOptions) => {
    return (target: object, key: string) => {
        IsDateString(
            {},
            {
                ...validationOptions,
                message: validationOptions?.each
                    ? 'Каждый элемент массива должен быть корректной датой в формате YYYY-MM-DD'
                    : validationOptions?.message ??
            'Поле должно быть корректной датой в формате YYYY-MM-DD',
            },
        )(target, key);
    };
};
