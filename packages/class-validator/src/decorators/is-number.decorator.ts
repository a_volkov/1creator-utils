import { IsNumber, IsNumberOptions, ValidationOptions } from 'class-validator';

export const IsNumberI18n = (
    opts?: IsNumberOptions,
    validationOptions?: ValidationOptions,
) => {
    return (target: object, key: string) => {
        IsNumber(opts, {
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен быть числом'
                : validationOptions?.message ?? 'Поле должно быть числом',
        })(target, key);
    };
};
