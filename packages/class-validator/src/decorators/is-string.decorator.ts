import { IsString, ValidationOptions } from 'class-validator';

export const IsStringI18n = (validationOptions?: ValidationOptions) => {
    return (target: object, key: string) => {
        IsString({
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен быть строкой'
                : validationOptions?.message ?? 'Поле должно быть строкой',
        })(target, key);
    };
};
