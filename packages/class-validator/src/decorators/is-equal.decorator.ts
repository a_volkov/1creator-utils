import { Equals, ValidationOptions } from 'class-validator';

export const EqualsI18n = (
    comparison: string,
    validationOptions?: ValidationOptions,
) => {
    return (target: object, key: string) => {
        Equals(comparison, {
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен быть равен $constraint1'
                : validationOptions?.message ??
          'Поле должно быть равно $constraint1',
        })(target, key);
    };
};
