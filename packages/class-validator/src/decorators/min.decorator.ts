import { Min, ValidationOptions } from 'class-validator';

export const MinI18n = (min: number, validationOptions?: ValidationOptions) => {
    return (target: object, key: string) => {
        Min(min, {
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен быть больше, чем $constraint1'
                : validationOptions?.message ??
          'Поле должно быть больше, чем $constraint1',
        })(target, key);
    };
};
