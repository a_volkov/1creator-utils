import { IsNotEmpty, ValidationOptions } from 'class-validator';

export const IsNotEmptyI18n = (validationOptions?: ValidationOptions) => {
    return (target: object, key: string) => {
        IsNotEmpty({
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен быть заполнен'
                : validationOptions?.message ??
                'Поле должно быть заполнено',
        })(target, key);
    };
};
