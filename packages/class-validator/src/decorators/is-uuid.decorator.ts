import { IsUUID, ValidationOptions } from 'class-validator';
import { UUIDVersion } from 'validator/lib/isUUID';

export const IsUuidI18n = (
    opts?: UUIDVersion,
    validationOptions?: ValidationOptions,
) => {
    return (target: object, key: string) => {
        IsUUID(opts, {
            ...validationOptions,
            message: validationOptions?.each
                ? 'Каждый элемент массива должен быть UUID'
                : validationOptions?.message ?? 'Поле должно быть в формате UUID',
        })(target, key);
    };
};
