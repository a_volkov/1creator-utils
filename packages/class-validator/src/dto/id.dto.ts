import { IsIntI18n } from '../decorators/is-int.decorator';
import { ApiProperty } from '@nestjs/swagger';

export class IdDto {
    @ApiProperty()
    @IsIntI18n()
    id: number;
}