import { IsUuidI18n } from '../decorators/is-uuid.decorator';
import { ApiProperty } from '@nestjs/swagger';

export class UuidDto {
    @ApiProperty()
    @IsUuidI18n()
    uuid: string;
}