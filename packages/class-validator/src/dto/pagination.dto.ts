import { IsOptional } from 'class-validator';
import { IsIntI18n } from '../decorators/is-int.decorator';
import { ApiProperty } from '@nestjs/swagger';

export class PaginationDto {
    @ApiProperty({
        required: false,
        default: 0,
    })
    @IsOptional()
    @IsIntI18n()
    offset?: number = 0;

    @ApiProperty({
        required: false,
        default: 10,
    })
    @IsOptional()
    @IsIntI18n()
    limit?: number = 10;
}