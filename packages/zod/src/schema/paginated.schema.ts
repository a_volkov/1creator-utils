import { z } from 'nestjs-zod/z';
import { createZodDto } from 'nestjs-zod';

export const PaginatedSchema = z.object({
    limit: z.number().int().default(30).optional(),
    offset: z.number().int().default(0).optional(),
});


export type PaginatedRequest = z.infer<typeof PaginatedSchema>;

export type PaginatedResponse<T> = {
    items: T[],
    count: number
};

export class PaginatedDto extends createZodDto(PaginatedSchema) {
}