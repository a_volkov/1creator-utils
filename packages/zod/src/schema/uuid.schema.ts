import { z } from 'nestjs-zod/z';
import { createZodDto } from 'nestjs-zod';

export const UuidSchema = z.object({ uuid: z.string().uuid() });

export type UuidRequest = z.infer<typeof UuidSchema>;

export class UuidDto extends createZodDto(UuidSchema) {
}