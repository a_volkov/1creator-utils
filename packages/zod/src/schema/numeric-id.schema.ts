import { z } from 'nestjs-zod/z';
import { createZodDto } from 'nestjs-zod';

export const NumericIdSchema = z.object({ id: z.number().int() });

export type NumericIdRequest = z.infer<typeof NumericIdSchema>;

export class NumericIdDto extends createZodDto(NumericIdSchema) {
}