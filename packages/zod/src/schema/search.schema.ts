import { z } from 'nestjs-zod/z';
import { createZodDto } from 'nestjs-zod';


export const SearchSchema = z.object({ search: z.string().optional() });

export type SearchRequest = z.infer<typeof SearchSchema>;

export class SearchDto extends createZodDto(SearchSchema) {
}