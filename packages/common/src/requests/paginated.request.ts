export class PaginatedRequest {
    limit?: number = 10;
    offset?: number = 0;

    constructor(limit = 10, offset = 0) {
        this.limit = limit;
        this.offset = offset;
    }
}
