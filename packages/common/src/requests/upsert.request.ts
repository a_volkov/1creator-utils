export class UpsertRequest<T> {
    items: Array<T> = [];
    removeIds: any[] = [];

    constructor(items: T[], removeIds: any[] = []) {
        this.items = items;
        this.removeIds = removeIds;
    }
}
