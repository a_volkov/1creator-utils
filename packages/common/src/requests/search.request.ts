export class SearchRequest {
    search?: string;

    constructor(search?: string) {
        this.search = search;
    }
}
