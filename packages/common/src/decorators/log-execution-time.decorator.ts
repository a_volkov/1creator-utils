const perf = performance || (require('perf_hooks')).performance;

export const LogExecutionTime = (
    target: unknown,
    propertyKey: string,
    descriptor: PropertyDescriptor,
) => {
    const originalMethod = descriptor.value;

    descriptor.value = async function(...args: any[]) {
        const start = perf.now();
        const result = await originalMethod.apply(this, args);
        const finish = perf.now();
        console.log(propertyKey + ` execution time: ${finish - start} milliseconds`);
        return result;
    };
};