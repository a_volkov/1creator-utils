export type PaginatedResponse<T> = {
    items: Array<T>;
    count: number;
}
