export function formatPhone(s: string, plus = true) {
    if (!s) {
        return s;
    }

    const startsWith = plus ? '+7' : '8';

    let phone = s.replace(/[^0-9*]/g, '');
    if (phone.startsWith('7') && plus) {
        phone = phone.slice(1);
    } else if (phone.startsWith('8')) {
        phone = phone.slice(1);
    }

    return phone.replace(/(.{3})(.{3})(.{2})(.{2})/g, `${startsWith}($1)$2-$3$4`);
}