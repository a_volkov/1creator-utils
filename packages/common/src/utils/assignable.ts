import { ExcludeMethods } from '../types/exclude-methods';

export abstract class Assignable {
    assign(v?: Partial<ExcludeMethods<this>>): this {
        v && Object.assign(this, JSON.parse(JSON.stringify(v)));
        return this;
    }

    toJSON() {
        return { ...this };
    }
}