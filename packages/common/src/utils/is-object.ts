export function isObject(val:any) {
    return val && typeof val === 'object';
}
