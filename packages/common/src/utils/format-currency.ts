const formatter = new Intl.NumberFormat('ru-RU', { maximumFractionDigits: 2 });


export function FormatCurrency(value: number) {
    return formatter.format(value);
}
