export function pick<O extends object, T extends keyof O>(obj: O, keys: T[]): Pick<O, T> {
    const ret: any = {};
    keys.forEach(key => {
        ret[key] = obj[key];
    });
    return ret;
}