export function omit<O extends object, T extends keyof O>(obj: O, keys: T[]): Omit<O, T> {
    const ret: any = {};
    (Object.keys(obj) as (keyof typeof obj)[]).forEach(key => {
        if (!keys.includes(key as any)) {
            ret[key] = obj[key];
        }
    });
    return ret;
}