export type PartialPick<T, Keys extends keyof T> = Partial<T> & Pick<T, Keys>
