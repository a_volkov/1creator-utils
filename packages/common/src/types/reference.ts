export type Reference<T> =
  T extends { id: infer U } ? (Partial<T> & { id: U })
    : T extends { uuid: infer U } ? (Partial<T> & { uuid: U })
      : never
