export type PaginateParams = {
    limit?: number,
    offset?: number
}
