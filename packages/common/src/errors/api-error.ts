type Scalar = boolean | number | string | bigint | symbol | Date | null | undefined;

export type ApiErrorDetails<T> = {
    [P in keyof T]: T[P] extends Scalar ? string[] : T[P] extends Array<infer U> ? ApiErrorDetails<U>[]
        : T[P] extends Record<string, any> ? ApiErrorDetails<T[P]> : string[]
}

export class ApiError<T = undefined> {
    statusCode: number;
    name: string;
    message: string;
    details: ApiErrorDetails<Required<T>>;

    constructor(val: Partial<ApiError>) {
        Object.assign(this, val);
    }
}
