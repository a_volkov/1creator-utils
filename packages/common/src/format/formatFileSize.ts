export function formatFileSize(val: number) {
    let res = val / 1024;
    let unit = 'КБ';

    if (res >= 1024) {
        res /= 1024;
        unit = 'МБ';
    }
    if (res >= 1024) {
        res /= 1024;
        unit = 'ГБ';
    }
    if (res >= 1024) {
        res /= 1024;
        unit = 'ТБ';
    }
    return `${res.toFixed(2)} ${unit}`;
}
