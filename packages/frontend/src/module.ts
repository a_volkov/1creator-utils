import { addComponentsDir, addImportsDir, addPlugin, createResolver, defineNuxtModule } from '@nuxt/kit';

export interface IModuleOptions {
    colors: string[]
}

export default defineNuxtModule<IModuleOptions>({
    meta: { name: '@1creator/frontend' },
    defaults: {
        colors: [
            'blank',
            'easy',
            'plain',
            'primary',
            'secondary',
            'neutral',
            'danger',
            'warning',
            'success',
        ],
    },
    async setup(options, nuxt) {
        const { resolve } = createResolver(import.meta.url);
        const runtimeDir = resolve('./runtime');
        const isDevelopment = runtimeDir.endsWith('src/runtime') || runtimeDir.endsWith('src\\runtime');

        addImportsDir(resolve('runtime/composables'));
        addImportsDir(resolve('runtime/utils'));

        addPlugin(resolve('runtime/plugin'));

        await addComponentsDir({
            prefix: 'cr',
            pathPrefix: false,
            pattern: '**/*.vue',
            isAsync: true,
            path: resolve('runtime/components'),
            transpile: false,
        });

        const cssFile = isDevelopment ? resolve(runtimeDir, 'scss/index.scss') : resolve(runtimeDir, 'scss/index.css');
        nuxt.options.css.splice(0, 0, cssFile);

        nuxt.hook('nitro:config', (nitroConfig) => {
            nitroConfig.publicAssets ||= [];
            nitroConfig.publicAssets.push({
                dir: resolve('./runtime/public'),
                maxAge: 60 * 60 * 24 * 365, // 1 year
            });
        });
    },
});
