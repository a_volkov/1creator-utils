import { computed, reactive, type Ref, ref } from 'vue';
import { useRoute, useRouter } from 'vue-router';
import type { Paginated, PaginateParams } from '@1creator/common';
import { useLazyAsyncData } from '#imports';

export interface IPaginator<T = any> {
    pending: boolean;
    limit: number;
    offset: number;
    readonly page: number;
    readonly count: number;
    readonly pagesCount: number;
    reload: () => Promise<Paginated<T>>;
    reset: () => Promise<void>;
    more: () => Promise<void>;
    go: (page: number) => Promise<void>;
}

export interface IPaginatorOptions<T> {
    limit?: number,
    offset?: number,
    pageParameter?: string,
    immediate?: boolean,
    fetch: (opts: PaginateParams) => Promise<Paginated<T>>
}

export function usePagination<T>(key: string, options: IPaginatorOptions<T>): {
    data: Ref<Paginated<T>>,
    paginator: IPaginator<T>
} {
    const limit = ref(options.limit || 10);
    const offset = ref(options.offset || 0);

    const pageParameter = options.pageParameter === undefined ? 'page' : options.pageParameter;
    const route = useRoute();
    const router = useRouter();

    if (pageParameter) {
        const page = route.query[pageParameter];
        if (page) {
            offset.value = (page ? +page - 1 : 0) * limit.value;
        }
    }

    const {
        data,
        pending,
        execute: reload,
    } = useLazyAsyncData<Paginated<T>>(
        'paginator.' + key,
        () => {
            return options.fetch({
                offset: offset.value,
                limit: limit.value,
            });
        },
        {
            immediate: options.immediate !== false,
            default() {
                return {
                    items: [],
                    count: 0,
                };
            },
        });

    const more = async() => {
        if (pending.value || (offset.value + limit.value > data.value.count)) {
            return;
        }

        pending.value = true;
        offset.value += limit.value;
        const res = await options.fetch({
            offset: offset.value,
            limit: limit.value,
        });
        data.value.items.push(...res.items);
        pending.value = false;
    };

    const go = async(page: number) => {
        if (pending.value || (page * limit.value > data.value.count)) {
            return;
        }
        offset.value = page * limit.value;
        if (pageParameter) {
            await router.push({
                query: {
                    ...route.query,
                    [pageParameter]: page > 0 ? page + 1 : undefined,
                },
            });
        }
        await reload();
    };

    const page = computed(() => Math.ceil(offset.value / limit.value));
    const count = computed(() => data.value.count);
    const pagesCount = computed(() => Math.ceil(data.value.count / limit.value));

    const reset = () => go(0);

    const paginator: IPaginator<T> = reactive({
        pending,
        count,
        pagesCount,
        limit,
        offset,
        page,
        reload,
        reset,
        more,
        go,
    });

    return {
        data,
        paginator,
    };
}
