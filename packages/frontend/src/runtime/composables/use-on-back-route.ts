import { navigateTo, onMounted, onUnmounted, useRoute, watch } from '#imports';

export function useOnBackRoute(callback: () => void) {
    const route = useRoute();
    const hash = (Math.random() + 1).toString(36).substring(7);

    const removeHash = async() => {
        if (route.query[hash]) {
            await navigateTo({
                query: {
                    ...route.query,
                    [hash]: undefined,
                },
                replace: true,
            });
        }
    };

    onMounted(async() => {
        await navigateTo({
            query: {
                ...route.query,
                [hash]: 1,
            },
        });
    });

    watch(() => route.query[hash], (val) => {
        if (!val) {
            callback();
        }
    });

    onUnmounted(removeHash);

    return { removeHash };
}
