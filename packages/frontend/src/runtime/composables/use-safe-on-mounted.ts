import type { Ref } from 'vue';
import { onMounted } from '#imports';

// workaround for https://github.com/vuejs/core/issues/5844
export function useSafeOnMounted(element: Ref<HTMLElement | undefined>, listener: () => void, checkTimeLimit = 500) {
    if (typeof window !== 'undefined') {
        const checkInterval = 100;
        let checksLeft = checkTimeLimit / checkInterval;

        const check = () => {
            if (element.value?.isConnected) {
                listener();
            } else if (checksLeft > 0) {
                setTimeout(check, checkInterval);
                checksLeft--;
            }
        };

        onMounted(() => {
            check();
        });
    }
}
