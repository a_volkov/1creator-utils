import { onMounted, ref, type Ref, unref } from 'vue';

export type UseDraggableOptions = {
    dragDelay: number,
    handler?: string,
    onStart: () => void,
    onStop: () => void,
}

function makeGhost(element: HTMLElement, onStop: () => void) {
    const rect = element.getBoundingClientRect();
    const ghost = element.cloneNode(true) as HTMLElement;

    ghost.id = '';
    ghost.style.position = 'fixed';
    ghost.style.zIndex = '100000';
    ghost.style.width = rect.width + 'px';
    ghost.style.transformOrigin = 'center';
    ghost.style.transition = 'transform .2s ease-in';
    ghost.style.pointerEvents = 'none';
    element.style.opacity = '0';

    let frameRequest: number;

    const position = {
        x: rect.left,
        y: rect.top,
    };

    const updateGhostPosition = () => {
        window.cancelAnimationFrame(frameRequest);
        frameRequest = window.requestAnimationFrame(() => {
            ghost.style.left = position.x + 'px';
            ghost.style.top = position.y + 'px';
        });
    };

    document.body.append(ghost);

    let prevCoords: {
        x: number,
        y: number
    } | undefined;

    const onMove = (event: PointerEvent) => {
        const coords = {
            x: event.pageX,
            y: event.pageY,
        };

        if (prevCoords) {
            position.x += coords.x - prevCoords.x;
            position.y += coords.y - prevCoords.y;
            updateGhostPosition();
        }

        prevCoords = coords;
    };

    const onUp = () => {
        document.body.removeEventListener('pointermove', onMove);

        ghost.style.transition = 'all .2s ease-in';
        const rect = element.getBoundingClientRect();
        position.x = rect.left;
        position.y = rect.top;
        updateGhostPosition();
        ghost.style.transform = 'scale(1)';
        element.style.opacity = '1';
        setTimeout(() => {
            ghost.remove();
        }, 250);
        onStop();
    };

    document.body.addEventListener('pointermove', onMove, { passive: true });

    document.body.addEventListener('pointerup', onUp, { once: true });

    updateGhostPosition();
    window.requestAnimationFrame(() => {
        ghost.style.transform = 'scale(0.8)';
    });

    return ghost;
}

export function useDraggable(
    element: Ref<HTMLElement> | HTMLElement | string | (() => HTMLElement),
    opts?: Partial<UseDraggableOptions>,
) {
    const disabled = ref(false);

    onMounted(() => {
        const options: UseDraggableOptions = {
            dragDelay: 300,
            onStart() {
            },
            onStop() {
            },
            ...opts,
        };
        let dragDelayTimer: NodeJS.Timeout;
        let el: HTMLElement;

        if (typeof element === 'string') {
            el = document.querySelector(element)!;
        } else if (typeof element === 'function') {
            el = element();
        } else {
            el = unref(element);
        }

        const handler = options.handler ? el.querySelector(options.handler)! : el;

        if (!handler) {
            return;
        }

        handler.addEventListener('mousedown', () => {
            if (disabled.value) {
                return;
            }
            dragDelayTimer = setTimeout(() => {
                options.onStart();
                makeGhost(el, options.onStop);
            }, options.dragDelay);
        });

        handler.addEventListener('mouseup', () => {
            if (disabled.value) {
                return;
            }
            clearTimeout(dragDelayTimer);
        });
    });

    return { disabled };
}
