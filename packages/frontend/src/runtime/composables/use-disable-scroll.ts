import { onMounted, onUnmounted } from '#imports';

export function useDisableScroll() {
    let scrollWidth: number;

    onMounted(() => {
        scrollWidth = window.innerWidth - document.body.clientWidth;
        document.body.style.overflow = 'hidden';
        document.body.style.paddingRight = `${scrollWidth}px`;
    });

    onUnmounted(() => {
        document.body.style.removeProperty('overflow');
        document.body.style.removeProperty('padding-right');
    });
}
