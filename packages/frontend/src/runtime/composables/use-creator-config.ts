import { defu } from 'defu';

type DeepPartial<T> = T extends object ? {
    [P in keyof T]?: DeepPartial<T[P]>;
} : T;

export type CreatorConfig = {
    drawer: {
        size: string
        direction: 'rtl' | 'ltr' | 'btt'
    },
    upload: {
        uploadFunction: (v: FormData) => Promise<any>,
        thumbnailGetter: (v: any) => string,
    },
    tiptap: {
        uploadFunction: (v: FormData) => Promise<{
            url: string
        }>,
    },
}

const config: CreatorConfig = {
    drawer: {
        size: '1000px',
        direction: 'rtl',
    },
    upload: {
        uploadFunction: (v: FormData) => {
            throw new Error('upload function not set');
        },
        thumbnailGetter: (v: any) => {
            throw new Error('thumbnail getter not set');
        },
    },
    tiptap: {
        uploadFunction: (v: FormData) => {
            throw new Error('upload function not set');
        },
    },
};

export function useCreatorConfig(configChanges?: DeepPartial<CreatorConfig>) {
    if (configChanges) {
        Object.assign(config, defu(configChanges, config));
    }
    return config;
}
