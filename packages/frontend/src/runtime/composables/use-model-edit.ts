import { ApiError } from '@1creator/common';
import type { ComputedRef, Ref } from 'vue';
import { ref } from 'vue';
import { $confirm, $notify, computed, useLazyAsyncData } from '#imports';

type UseModelEditOptions<T, E> = {
    get?: (v: any) => Promise<any>,
    store?: (v: any) => Promise<any>,
    update?: (v: any) => Promise<any>,
    remove?: (v: any) => any,
    onGet?: (v: Partial<T>) => void,
    onStore?: (v: T) => void,
    onUpdate?: (v: T) => void,
    onError?: (v: ApiError<E>) => void,
    onRemove?: (v: T) => void,
    isExistsFnc?: () => boolean,
    saveConfirmationText?: string | boolean
    removeConfirmationText?: string | boolean
}

function isExists(val: any): boolean {
    return val && (val.id || val.uuid);
}

export function useModelEdit<T, E = T>(
    val: Partial<T>,
    opts: UseModelEditOptions<T, E>,
    asyncDataKey?: string,
): {
    params: Ref<T>,
    error: Ref<ApiError<E> | undefined>,
    saving: Ref<boolean>,
    removing: Ref<boolean>,
    pending: ComputedRef<boolean>,
    fetching: Ref<boolean>,
    save: () => Promise<void>,
    remove: () => Promise<void>,
} {
    let params: Ref<T>;
    const isExistsFunc = opts.isExistsFnc || isExists;
    const saving: Ref<boolean> = ref(false);
    const removing: Ref<boolean> = ref(false);
    let fetching: Ref<boolean> = ref(false);
    const error = ref<ApiError<E> | undefined>();

    const pending = computed(() => {
        return saving.value || removing.value || fetching.value;
    });

    const removeConfirmationText = opts.removeConfirmationText === false
        ? false
        : (opts.removeConfirmationText ?? 'Это действие нельзя отменить. Подтвердите удаление.');

    const saveConfirmationText = opts.saveConfirmationText === false
        ? false
        : (opts.saveConfirmationText ?? 'Изменения успешно сохранены!');

    if (opts.get && isExistsFunc(val)) {
        const key = asyncDataKey || (val.constructor?.name + JSON.stringify(val));
        const data = useLazyAsyncData(
            key,
            async() => {
                const res = await opts.get!(val);
                opts.onGet?.(val);
                return res;
            },
            {
                default() {
                    return val;
                },
            },
        );
        params = data.data as Ref<T>;
        // eslint-disable-next-line
        fetching = data.pending;
    } else {
        params = ref(JSON.parse(JSON.stringify(val))) as Ref<T>;
        fetching.value = false;
    }

    const save = async() => {
        saving.value = true;
        error.value = undefined;
        try {
            if (isExistsFunc(params.value) && opts.update) {
                params.value = await opts.update(params.value);
                opts.onUpdate?.(params.value);
            } else if (opts.store) {
                params.value = await opts.store(params.value);
                opts.onStore?.(params.value);
            }
            if (saveConfirmationText) {
                $notify({
                    type: 'success',
                    body: saveConfirmationText as string,
                    title: 'Отлично!',
                });
            }
        } catch (e) {
            console.log(e);
            error.value = e as ApiError<E>;
            saving.value = false;
            opts.onError?.(error.value);
            throw e;
        }
        saving.value = false;
    };

    const remove = async() => {
        if (removeConfirmationText) {
            try {
                await $confirm(removeConfirmationText as string);
            } catch (e) {
                return;
            }
        }

        removing.value = true;
        error.value = undefined;
        try {
            await opts.remove!(params.value);
            opts.onRemove?.(params.value);
        } catch (e) {
            if (e instanceof ApiError) {
                console.error(e);
                error.value = e as ApiError<E>;
            }
            removing.value = false;
            throw e;
        }
        removing.value = false;
    };

    return {
        params,
        error,
        saving,
        removing,
        fetching,
        pending,
        save,
        remove,
    };
}
