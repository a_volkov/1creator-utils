import type { LocationQuery, LocationQueryRaw } from 'vue-router';
import { navigateTo, reactive, useLazyAsyncData, useRoute, watch } from '#imports';

export async function useReactiveQuery<Params extends Record<string, any>>(
    opts?: {
        default?: Partial<{ [P in keyof Params]: Params[P] | null | undefined }>,
        serializer?: (v: Partial<Params>) => LocationQueryRaw,
        parser?: (v: LocationQuery) => Promise<any> | any,
    },
) {
    const route = useRoute();

    const {
        pending,
        data,
    } = await useLazyAsyncData(JSON.stringify(route.query), async() => {
        return opts?.parser
            ? await opts.parser(route.query)
            : route.query;
    });

    const query: Params = reactive({ ...opts?.default, ...data.value } as any);

    const unwatch = watch(data, () => {
        Object.assign(query, data.value);
        unwatch();
    }, {});

    watch(query, async() => {
        const newQuery = opts?.default
            ? Object.keys(query).reduce((acc, param: keyof Params) => {
                acc[param] = query[param] === opts.default![param] ? undefined : query[param];
                return acc;
            }, {} as Params)
            : query;

        const serialized = opts?.serializer ? opts.serializer(newQuery) : newQuery;

        await navigateTo({
            query: {
                ...useRoute().query,
                ...serialized,
            },
        });
    });

    function reset() {
        for (const i in query) {
            delete query[i];
        }

        if (opts?.default) {
            Object.assign(query, opts.default);
        }
    }

    return {
        query,
        reset,
        pending,
    };
}
