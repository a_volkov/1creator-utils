export function useShare(url: string, text: string) {
    const joinUrlEncoded = url ? encodeURIComponent(url) : '';
    const textEncoded = text ? encodeURIComponent(text) : '';

    const telegram = `https://t.me/share/url?url=${joinUrlEncoded}&text=${textEncoded}`;
    const whatsapp = `https://wa.me/?text=${textEncoded}%0A%0A${joinUrlEncoded}`;
    const vk = `https://vkontakte.ru/share.php?url=${joinUrlEncoded}&description=${textEncoded}`;
    const viber = `viber://forward?text=${joinUrlEncoded}&title=${textEncoded}`;
    const qr = `https://api.qrserver.com/v1/create-qr-code/?size=250x250&data=${joinUrlEncoded}`;

    return {
        telegram,
        whatsapp,
        vk,
        viber,
        qr,
    };
}
