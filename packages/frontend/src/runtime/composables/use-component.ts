import { Component, createApp } from 'vue';

export function useClosableComponent(componentClass: Component, rootProps?: Record<string, unknown>) {
    const wrapper = document.createElement('div');

    const instance = createApp(componentClass, {
        onClose() {
            document.body.removeChild(wrapper);
        },
        ...rootProps,
    });

    instance.mount(wrapper);

    document.body.appendChild(wrapper);
}
