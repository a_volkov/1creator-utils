import type { Ref } from 'vue';
import { computed } from 'vue';
import { $notify, onBeforeUnmount, ref, useCreatorConfig } from '#imports';

export class UploadingItem {
    progress = 0;
    error?: Error;
    file: File;

    constructor(file: File) {
        this.file = file;
    }
}

export function useUpload<T>(params: {
    modelValue?: T | T[],
    maxCount?: number,
    accept?: string[] | 'image' | null,
    uploadFunction?: (v: FormData) => Promise<T>,
    onUploadFinish?: (v: T) => Promise<any> | any,
    onUploadError?: (v: any) => Promise<any> | any,
}) {
    const config = useCreatorConfig();
    const uploadedItems = ref([]) as Ref<T[]>;
    const uploadingItems = ref<UploadingItem[]>([]);
    const uploadFunction = params.uploadFunction || config.upload.uploadFunction;
    const maxCount = params.maxCount || 1;

    if (params.modelValue) {
        if (Array.isArray(params.modelValue)) {
            uploadedItems.value.push(...params.modelValue);
        } else {
            uploadedItems.value.push(params.modelValue);
        }
    }

    const isUnmounted = ref(false);
    const isUploading = ref(false);

    const accept = computed(() => {
        if (params.accept === 'image') {
            return ['.png', '.jpg', '.jpeg', '.webp'];
        } else {
            return params.accept;
        }
    });

    const canUpload = computed(() => {
        const totalCount = uploadedItems.value.length + uploadingItems.value.length;
        return (maxCount > 1 && totalCount < maxCount) || !totalCount;
    });

    const uploadFiles = (files: FileList) => {
        for (let i = 0; i < files.length && uploadedItems.value.length < maxCount; i++) {
            const file = files[i];

            if (accept.value && !accept.value.includes('*.*')) {
                const fileExtension = '.' + file.name.split('.').pop()?.toLowerCase();
                if (!accept.value.includes(fileExtension)) {
                    $notify({
                        type: 'error',
                        title: 'Ошибка',
                        body: 'Допустимый формат файлов: ' + accept.value.join(', '),
                    });
                    continue;
                }
            }
            uploadingItems.value.push(new UploadingItem(file));
        }

        uploadNextFile().then();
    };

    const uploadNextFile = async() => {
        const file: UploadingItem | undefined = uploadingItems.value.find(i => !i.error);
        if (!file || isUploading.value || isUnmounted.value) {
            return;
        }

        isUploading.value = true;
        const formData = new FormData();
        formData.append('file', file.file);
        try {
            if (uploadFunction) {
                const upload = await uploadFunction(formData);
                if (params.onUploadFinish) {
                    await params.onUploadFinish(upload);
                }
                uploadedItems.value.push(upload);
            }
        } catch (e) {
            console.error(e);
            await params.onUploadError?.(e);
        }
        const idx = uploadingItems.value.indexOf(file);
        uploadingItems.value.splice(idx, 1);
        isUploading.value = false;

        await uploadNextFile();
    };

    const clear = () => {
        uploadedItems.value = [];
        uploadingItems.value = [];
    };

    onBeforeUnmount(() => {
        isUnmounted.value = true;
    });

    return {
        accept,
        canUpload,
        uploadFiles,
        clear,
        uploadedItems,
        uploadingItems,
    };
}
