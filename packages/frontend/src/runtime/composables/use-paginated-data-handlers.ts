import type { Ref } from 'vue';
import type { PaginatedResponse } from '@1creator/common';

export const usePaginatedDataHandlers = function <T = any>(
    data: Ref<null | PaginatedResponse<T>>,
    key: keyof T = 'id' as keyof T,
) {
    const onStore = (val: T) => {
        if (!data.value) {
            return;
        }

        data.value.items.splice(0, 0, val);
        data.value.count++;
    };

    const onRemove = (val: T) => {
        if (!data.value) {
            return;
        }

        const idx = data.value.items.findIndex(i => i[key] === val[key]);
        if (idx >= 0) {
            data.value.items.splice(idx, 1);
            data.value.count--;
        }
    };

    const onUpdate = (val: T) => {
        if (!data.value) {
            return;
        }

        const idx = data.value.items.findIndex(i => i[key] === val[key]);
        if (idx >= 0) {
            data.value.items.splice(idx, 1, val);
        }
    };

    return {
        onStore,
        onRemove,
        onUpdate,
    };
};
