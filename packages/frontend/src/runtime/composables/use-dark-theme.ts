import { useHead, useStatefulCookie } from '#imports';

export function useDarkTheme(opts?: {
    class?: string
}) {
    const darkThemeClass = opts?.class || '--theme-dark';
    const enabled = useStatefulCookie('darkThemeEnabled', {
        path: '/',
        maxAge: 60 * 60 * 24 * 365 * 5,
        default() {
            return false;
        },
    });

    function setup() {
        useHead({
            htmlAttrs: {
                class: () => {
                    const res: string[] = [];
                    if (enabled.value) {
                        res.push(darkThemeClass);
                    }
                    return res;
                },
            },
        });
    }

    return {
        enabled,
        setup,
    };
}
