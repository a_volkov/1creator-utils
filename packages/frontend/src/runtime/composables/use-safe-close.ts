import { toValue } from 'vue';
import { $confirm } from '../utils/msgbox';

export function useSafeClose(data: any, msg = 'Введённые данные не будут сохранены. Вы подтверждаете закрытие?') {
    let savedData = '';

    const setSafeData = () => {
        savedData = JSON.stringify(toValue(data));
    };
    setSafeData();

    return {
        async beforeClose() {
            if (JSON.stringify(toValue(data)) !== savedData) {
                await $confirm(msg);
            }
        },
        setSafeData,
    };
}
