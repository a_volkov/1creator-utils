import { ref } from 'vue';
import { $notify } from '../utils/notify';

export function useClipboard(opts = {
    resetTime: 3000,
    copyNotificationText: 'Данные скопированы в буфер обмена',
}) {
    const copied = ref(false);

    async function write(text: string) {
        await navigator.clipboard.writeText(text);
        copied.value = true;

        if (opts.copyNotificationText) {
            $notify({
                type: 'success',
                title: 'Выполнено',
                body: opts.copyNotificationText,
            });
        }

        if (opts.resetTime) {
            setTimeout(() => copied.value = false, opts.resetTime);
        }
    }

    async function read() {
        return await navigator.clipboard.readText();
    }

    return {
        copied,
        write,
        read,
    };
}
