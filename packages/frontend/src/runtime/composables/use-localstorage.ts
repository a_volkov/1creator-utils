import { onMounted, ref, type Ref, watch } from 'vue';

export function useLocalStorage<T>(key: string, defaultValue: any, validator?: (v: any) => boolean): Ref<T> {
    const res = ref(defaultValue);

    onMounted(() => {
        const rawValue = localStorage.getItem(key);

        watch(res, () => {
            localStorage.setItem(key, JSON.stringify(res.value));
        }, { deep: true });

        if (rawValue) {
            try {
                const parsed = JSON.parse(rawValue);
                if (!validator || validator(parsed)) {
                    res.value = parsed;
                }
            } catch (e) {
                console.log(`Failed to parse localstorage value: ${key}`);
            }
        }
    });

    return res;
}
