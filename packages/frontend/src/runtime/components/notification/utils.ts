import { App, ComponentPublicInstance, createApp } from 'vue';

import NotificationContainer from './NotificationContainer.vue';
import { INotification } from './interfaces';

let containerApp: App;
let containerComponent: ComponentPublicInstance;

const notify = (props: INotification) => {
    if (!process.client) {
        return;
    }

    if (!containerApp) {
        containerApp = createApp(NotificationContainer);
        const wrapper = document.createElement('div');
        containerComponent = containerApp.mount(wrapper);
        document.body.appendChild(wrapper);
    }

    // @ts-ignore
    containerComponent?.addNotification(props);
};

export { notify };
