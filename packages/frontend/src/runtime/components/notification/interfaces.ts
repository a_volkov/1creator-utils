export interface INotification {
    title: string,
    body: string,
    type?: 'info' | 'error' | 'warning' | 'success',
    duration?: number,
    closable?: boolean,
    onClose?: () => any,
    onClick?: () => any,
}
