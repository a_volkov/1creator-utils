<script setup lang="ts">
import { computed, ref } from 'vue';

const emit = defineEmits<{
    (e: 'update:modelValue', v: number | [number, number]): void
}>();

const props = withDefaults(defineProps<{
    modelValue?: number | [number, number],
    min?: number,
    max?: number,
    step?: number,
    disabled?: boolean,
    tooltip?: 'always' | 'hover' | false,
    marks?: boolean
}>(), {
    modelValue: 0,
    min: 0,
    max: 100,
    step: 1,
    disabled: false,
    tooltip: 'hover',
    marks: false,
});

const isRange = computed(() => Array.isArray(props.modelValue));

const naturalValues = computed(() => {
    return isRange.value
        ? [props.modelValue[0] ?? props.min, props.modelValue[1] ?? props.max]
        : [0, props.modelValue ?? props.max];
});

const fractionValues = computed(() => {
    return isRange.value
        ? [
            Math.max((naturalValues.value[0] - props.min) / (props.max - props.min), 0),
            Math.min((naturalValues.value[1] - props.min) / (props.max - props.min), 1),
        ]
        : [0, Math.max(Math.min((naturalValues.value[1] - props.min) / (props.max - props.min), 1), 0)];
});

const rangeStyle = computed(() => {
    return {
        width: (fractionValues.value[1] - fractionValues.value[0]) * 100 + '%',
        left: (fractionValues.value[0]) * 100 + '%',
    };
});

const handlesStyles = computed(() => {
    return [
        { left: (fractionValues.value[0]) * 100 + '%' },
        { left: (fractionValues.value[1]) * 100 + '%' },
    ];
});

const sliderEl = ref<HTMLElement>();

let draggingHandleIdx: number | undefined;

function onDragStart(e: MouseEvent | TouchEvent, handleIdx: number) {
    draggingHandleIdx = handleIdx;

    const sliderRect = sliderEl.value!.getBoundingClientRect();
    document.body.classList.toggle('cr-grabbing', true);

    const onMouseMove = (e: MouseEvent | TouchEvent) => {
        const clientCoords = e instanceof MouseEvent
            ? {
                x: e.clientX,
                y: e.clientY,
            }
            : {
                x: e.touches[0].clientX,
                y: e.touches[0].clientY,
            };

        let newValue = props.min + (props.max - props.min) * Math.max(Math.min((clientCoords.x - sliderRect.left) / (sliderRect.width), 1), 0);

        if (newValue > naturalValues.value[draggingHandleIdx]) {
            newValue = props.step * Math.floor(newValue / props.step);
        } else {
            newValue = props.step * Math.ceil(newValue / props.step);
        }

        const newValues = Array.from(naturalValues.value) as [number, number];
        newValues[draggingHandleIdx] = newValue;

        if (newValues[0] > newValues[1]) {
            newValues.sort();
            draggingHandleIdx ^= 1;
        }

        emit('update:modelValue', isRange.value ? newValues : newValues[1]);
    };

    const onMouseUp = () => {
        draggingHandleIdx = undefined;
        document.removeEventListener('mouseup', onMouseUp);
        document.removeEventListener('mousemove', onMouseMove);
        document.removeEventListener('touchmove', onMouseMove);
        document.body.classList.toggle('cr-grabbing', false);
    };

    document.addEventListener('mousemove', onMouseMove);
    document.addEventListener('touchmove', onMouseMove);
    document.addEventListener('mouseup', onMouseUp);
    document.addEventListener('touchend', onMouseUp);
}

function onMarkClick(val: number) {
    emit('update:modelValue', isRange.value ? [val, naturalValues.value[1]] : val);
}
</script>

<template>
    <div
        ref="sliderEl"
        class="cr-slider"
        :class="{
            'cr-slider--disabled': disabled,
            [`cr-slider--tooltip-${tooltip}`]: tooltip,
        }"
    >
        <div class="cr-slider__track-wrap">
            <div class="cr-slider__track">
                <span
                    class="cr-slider__range"
                    :style="rangeStyle"
                />
                <div
                    v-if="isRange"
                    ref="handleEl1"
                    draggable="false"
                    class="cr-slider__handle"
                    :style="handlesStyles[0]"
                    @mousedown="e => onDragStart(e, 0)"
                    @touchstart="e => onDragStart(e, 0)"
                >
                    <slot
                        name="tooltip"
                        :value="naturalValues[0]"
                    >
                        <div class="cr-slider__handle-tooltip">
                            {{ naturalValues[0] }}
                        </div>
                    </slot>
                </div>
                <div
                    ref="handleEl2"
                    draggable="false"
                    class="cr-slider__handle"
                    :style="handlesStyles[1]"
                    @mousedown="e => onDragStart(e, 1)"
                    @touchstart="e => onDragStart(e, 1)"
                >
                    <slot
                        name="tooltip"
                        :value="naturalValues[1]"
                    >
                        <div class="cr-slider__handle-tooltip">
                            {{ naturalValues[1] }}
                        </div>
                    </slot>
                </div>
            </div>
        </div>
        <div
            v-if="marks"
            class="cr-slider__marks"
        >
            <span
                v-for="(mark, idx) in (max - min + 1)"
                :key="idx"
                class="cr-slider__mark"
                :style="{left: `${100 * idx / (max - min)}%`}"
                @click="onMarkClick(min + mark - 1)"
            >
                {{ min + mark - 1 }}
            </span>
        </div>
    </div>
</template>

<style lang="scss">
.cr-grabbing * {
    cursor: grabbing !important;
}

.cr-slider {
    position: relative;
    touch-action: none;
}

.cr-slider--disabled {
    pointer-events: none;
    cursor: not-allowed;
    opacity: 0.5;
}

.cr-slider__track-wrap {
    height: 16px;
}

.cr-slider__track {
    background: #dcdcff;
    position: relative;
    left: 0;
    height: 6px;
    width: 100%;
    top: calc(50% - 2px);
}

.cr-slider__range {
    background: var(--primary-color);
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
}

.cr-slider__handle {
    width: 16px;
    height: 16px;
    user-select: none;
    border: 2px solid var(--primary-color);
    background: var(--easy-color);
    border-radius: 99px;
    cursor: grab;
    position: absolute;
    top: 50%;
    transform: translate(-50%, -50%);
    display: flex;
    justify-content: center;
    z-index: 2;

    &:hover, &:active {
        background: var(--primary-color);
        z-index: 3;
    }
}

.cr-slider__handle-tooltip {
    position: absolute;
    top: -7px;
    transform: translate(0, -100%);
    background: var(--plain-color);
    color: var(--plain-contrast-color);
    line-height: 1;
    padding: 5px;
    border-radius: 5px;
    opacity: 0;
    pointer-events: none;
    transition: opacity .1s ease-in;
    z-index: 5;
}

.cr-slider--tooltip-always {
    .cr-slider__handle-tooltip {
        opacity: 1;
    }
}

.cr-slider--tooltip-hover {
    .cr-slider__handle:active, .cr-slider__handle:hover {
        .cr-slider__handle-tooltip {
            opacity: 1;
        }
    }
}

.cr-slider__marks {
    text-align: center;
    position: relative;
    color: var(--neutral-color);
    height: 30px;
}

.cr-slider__mark {
    transform: translateX(-50%);
    position: absolute;
    padding-top: 1rem;
    top: -10px;
    cursor: pointer;

    &:hover {
        color: var(--plain-color);
    }

    &::before {
        content: '';
        display: block;
        width: 6px;
        height: 6px;
        background: var(--blank-color);
        border: var(--base-border-color);
        position: absolute;
        top: 0;
        left: 50%;
        transform: translateX(-50%);
    }
}
</style>
