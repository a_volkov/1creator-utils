import type { Ref } from 'vue';

export type TreeNode = {
    parent: any,
    item: any
    path: number[],
}

export type ProvidedTree = {
    draggableItem: Ref<TreeNode | undefined>,
    draggingOver: Ref<undefined | {
        node: TreeNode,
        type: 'before' | 'after' | 'inner'
    }>,
    move: (node: TreeNode, target: TreeNode, type: 'before' | 'after' | 'inner') => void
}
