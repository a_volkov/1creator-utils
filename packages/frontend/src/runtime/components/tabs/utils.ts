export interface ITabPane {
    name: string | number | null,
    label: string,
    disabled?: boolean,
    tab?: any,
    content: any
}

export const TABS_PROVIDE_KEY = 'tabs';
