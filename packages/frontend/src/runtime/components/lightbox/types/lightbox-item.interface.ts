export type LightboxItem = {
    slots: {
        slide?: any,
        thumbnail?: any,
        afterSlide?: any,
    },
}
