import { Ref } from 'vue';
import { type LightboxItem } from './lightbox-item.interface';

export type LightboxController = {
    close: () => void,
    show: (fromIdx?: number) => void,
    activeKey: Ref<number>,
    register: (item: LightboxItem) => number
    unregister: (key: number) => void
}
