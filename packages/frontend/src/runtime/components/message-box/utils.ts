import { createApp } from 'vue';
import MessageBox from './MessageBox.vue';

export interface IMessageBoxOptions {
    title: string,
    body: string,
    confirm?: boolean,
    confirmText?: string,
    cancelText?: string,
    handleBack?: boolean,
    type?: 'info' | 'error' | 'warning' | 'success',
}

const msgbox = (props: IMessageBoxOptions) => {
    return new Promise<void>((resolve, reject) => {
        const instance = createApp(MessageBox, {
            ...props,
            onClose() {
                instance.unmount();
                document.body.removeChild(wrapper);
                reject(new Error('Action cancelled by user'));
            },
            onConfirm() {
                resolve();
            },
        });
        const wrapper = document.createElement('div');
        instance.mount(wrapper);
        document.body.appendChild(wrapper);
    });
};

const confirm = (body: string, title = 'Требуется подтверждение', props?: Partial<IMessageBoxOptions>) => {
    return msgbox({
        confirm: true,
        title,
        body,
        ...props,
    });
};

export {
    msgbox,
    confirm,
};
