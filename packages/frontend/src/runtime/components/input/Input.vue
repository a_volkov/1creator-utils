<template>
    <div
        class="cr-input"
        :class="{
            'cr-input--error': isError,
            'cr-input--disabled': disabled,
            'cr-input--shift': isShift,
            'cr-input--focus': !disabled && !!isFocus,
        }"
    >
        <div
            class="cr-input__content"
            @click="inputEl!.focus();"
        >
            <div
                v-if="$slots.before"
                class="cr-input__before"
            >
                <slot name="before" />
            </div>
            <div class="cr-input__input-wrap">
                <input
                    ref="inputEl"
                    :value="modelValue"
                    class="cr-input__input"
                    :placeholder="placeholder"
                    :type="showPassword ? 'text' : type"
                    :step="step"
                    :disabled="disabled"
                    :required="required"
                    :min="min"
                    :max="max"
                    @input="onInput"
                    @change="onChange"
                    @focus="onFocus"
                    @blur="isFocus = false"
                >
                <label
                    v-if="label"
                    class="cr-input__label"
                >
                    {{ label }}
                </label>
                <slot name="inside" />
            </div>
            <div
                v-if="type === 'password' || $slots.after || nullable"
                class="cr-input__after"
            >
                <i
                    v-if="nullable && modelValue"
                    class="cr-icon-close cr-input__clear"
                    @click.stop="emit('update:modelValue', null)"
                />
                <i
                    v-if="type === 'password'"
                    class="link link--neutral"
                    :class="showPassword ? 'cr-icon-hide' : 'cr-icon-show'"
                    @click="showPassword = !showPassword"
                />
                <slot name="after" />
            </div>
        </div>
        <cr-transition-collapse>
            <div
                v-if="isError"
                class="cr-input__errors"
            >
                {{ errors.join('. ') }}
            </div>
        </cr-transition-collapse>
    </div>
</template>

<script setup lang="ts">
import { computed, ref, useSafeOnMounted } from '#imports';

const inputEl = ref<HTMLInputElement>();
const props = withDefaults(defineProps<{
    modelValue?: number | string | null,
    errors?: string[],
    type?: 'text' | 'password' | 'number' | 'email' | 'tel',
    label?: string,
    placeholder?: string,
    disabled?: boolean,
    required?: boolean,
    autofocus?: boolean,
    lazy?: boolean,
    nullable?: boolean,
    step?: number,
    min?: number,
    max?: number,
}>(), {
    modelValue: undefined,
    errors: undefined,
    type: 'text',
    label: undefined,
    placeholder: undefined,
    disabled: false,
    required: false,
    autofocus: false,
    lazy: false,
    nullable: false,
    step: undefined,
    min: undefined,
    max: undefined,
});

const emit = defineEmits<{
    (e: 'update:modelValue', v: number | string | null | undefined): void,
    (e: 'focus')
}>();

const isFocus = ref(false);
const showPassword = ref(false);
const isError = computed(() => props.errors?.length);

const isShift = computed(() => {
    const filled = ![undefined, null, ''].includes(props.modelValue as any);
    return ((isFocus.value || filled) && props.label) || !!(props.label && props.placeholder);
});

if (props.autofocus) {
    useSafeOnMounted(inputEl, focus);
}

function onFocus() {
    isFocus.value = true;
    emit('focus');
}

function focus() {
    inputEl.value?.focus();
}

function onInput(e: InputEvent) {
    if (!props.lazy) {
        emit('update:modelValue', (e.target as HTMLInputElement).value);
    }
}

function onChange(e: InputEvent) {
    emit('update:modelValue', (e.target as HTMLInputElement).value);
}

defineExpose({
    inputEl,
    focus,
});

</script>

<style lang="scss">
.cr-input--focus .cr-input__content {
    border-color: var(--primary-color);
}

.cr-input--error .cr-input__content {
    border-color: var(--danger-color);
}

.cr-input--disabled {
    cursor: not-allowed;
    opacity: 0.75;
}

.cr-input--shift {
    .cr-input__label {
        font-size: 75%;
        height: calc(var(--base-control-height) * 0.45);
    }

    .cr-input__input {
        padding-top: calc(var(--base-control-height) * 0.35);
    }
}

.cr-input__content {
    display: flex;
    height: var(--base-control-height);
    background: var(--input-background, var(--blank-color));
    border-radius: var(--base-border-radius);
    border: var(--base-border-width, 1px) solid var(--base-border-color);
    transition: border .25s;
    padding: 0 15px;
    gap: 15px;
    font: var(--input-font);
    position: relative;
}

.cr-input__input-wrap {
    position: relative;
    flex-grow: 1;
}

.cr-input__input {
    outline: none !important;
    color: var(--plain-color);
    appearance: none;
    font: var(--base-font);
    font-size: inherit;
    background: none;
    height: 100%;
    width: 100%;
    -moz-appearance: textfield;
    border: none;

    &::placeholder {
        font: var(--base-font);
        font-size: 1rem;
        color: var(--neutral-color);
    }

    &:-webkit-autofill {
        padding-top: 15px;

        ~ .cr-input__label {
            font-size: 0.80rem;
            line-height: 18px;
            height: 25px;
        }
    }

    &:-webkit-autofill,
    &:-webkit-autofill:hover,
    &:-webkit-autofill:focus,
    &:-webkit-autofill:active {
        -webkit-box-shadow: 0 0 0 30px var(--blank-color) inset !important;
        -webkit-text-fill-color: var(--blank-contrast-color) !important;
        font: var(--base-font) !important;
    }

    &::-webkit-outer-spin-button,
    &::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
}

.cr-input__input[disabled] {
    pointer-events: none;
}

.cr-input__label {
    margin-bottom: 0;
    white-space: nowrap;
    width: max-content;
    color: var(--neutral-color);
    position: absolute;
    transition: all .1s linear;
    cursor: text;
    pointer-events: none;
    font-size: inherit;
    max-width: 100%;
    overflow: hidden;
    text-overflow: ellipsis;
    height: 100%;
    top: 0;
    display: flex;
    align-items: center;
}

.cr-input__before, .cr-input__after {
    color: var(--neutral-color);
    display: flex;
    align-items: center;

    i {
        font-size: 1.5rem;
    }
}

.cr-input__errors {
    font-size: 0.85rem;
    color: var(--danger-color);
    width: 100%;
    text-align: left;
}

.cr-input__clear {
    position: absolute;
    background: var(--input-background, var(--blank-color));
    border: 1px solid var(--base-border-color);
    padding: 3px;
    border-radius: 50%;
    box-sizing: content-box;
    opacity: 0;
    pointer-events: none;
    right: 12px;
    transform: scale(1.5);
    transition: all 0.2s ease-in;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 18px !important;
    cursor: pointer;
}

.cr-input__clear:hover {
    color: var(--danger-color);
    border-color: var(--danger-color);
}

.cr-input:hover .cr-input__clear {
    opacity: 1;
    transform: scale(1);
    pointer-events: auto;
}
</style>
