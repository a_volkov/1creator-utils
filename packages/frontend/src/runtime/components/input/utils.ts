import { PropType } from '#imports';

export type InputType = 'text' | 'password' | 'number' | 'email' | 'tel';

export function buildProps<T extends Record<string, any>>(props: T): T {
    return props;
}

export const inputProps = buildProps({
    modelValue: {
        type: [Number, String],
        default: undefined,
    },
    errors: {
        type: Array as PropType<Array<string>>,
        default: undefined,
    },
    type: {
        type: String as PropType<InputType>,
        default: 'text',
    },
    label: {
        type: String,
        default: null,
    },
    placeholder: {
        type: String,
        default: null,
    },
    disabled: {
        type: Boolean,
        default: false,
    },
    required: {
        type: Boolean,
        default: false,
    },
    autofocus: {
        type: Boolean,
        default: false,
    },
    step: {
        type: Number,
        default: undefined,
    },
    min: {
        type: Number,
        default: undefined,
    },
    max: {
        type: Number,
        default: undefined,
    },
});
