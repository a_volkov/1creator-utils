<template>
    <div
        ref="rootEl"
        class="zoom-content"
        :class="{
            'zoom-content--grab': dragOnLeftMouse,
            'zoom-content--grabbing': isDragging,
            'zoom-content--grid': grid,
        }"
        @wheel="onWheel"
        @mousedown.middle.prevent="onMiddleMouseDown"
        @mouseup.middle="onMiddleMouseUp"
        @mousedown.left="onMouseDown"
        @mouseup.left="onMouseUp"
        @mousemove="onMouseMove"
        @touchstart="onTouchStart"
        @touchend="onTouchEnd"
        @touchmove="onTouchMove"
        @dblclick="onDblClick"
    >
        <div
            ref="workspaceContentEl"
            class="zoom-content__content"
            :style="contentStyle"
        >
            <slot />
        </div>
    </div>
</template>

<script lang="ts" setup>
import { computed, onMounted, reactive, type Ref, ref, watch } from 'vue';

const emit = defineEmits<{
    (e: 'transform', v: {
        scale: number,
        translate: {
            x: number,
            y: number
        }
    }): void
}>();

const props = withDefaults(defineProps<{
    dragOnLeftMouse?: boolean
    dragOnMiddleMouse?: boolean
    dragOnlyZoomed?: boolean
    initialCenter?: boolean,
    maxScale?: number,
    minScale?: number,
    bounds?: boolean,
    grid?: boolean,
    disabled?: boolean,
}>(), {
    dragOnLeftMouse: false,
    dragOnMiddleMouse: true,
    initialCenter: true,
    bounds: true,
    maxScale: 3,
    minScale: 1,
    grid: true,
    disabled: false,
});

const rootEl: Ref<HTMLElement | undefined> = ref();
const workspaceContentEl: Ref<HTMLElement | undefined> = ref();

const translate = reactive({
    x: 0,
    y: 0,
});
const scale = ref(1);

const contentStyle = computed(() => {
    return { transform: `translate3d(${translate.x}px, ${translate.y}px, 0) scale(${scale.value})` };
});

function zoom(origin: {
    x: number,
    y: number
}, zoom: number) {
    window.requestAnimationFrame(() => {
        const newScale = Math.max(Math.min(scale.value * zoom, props.maxScale), props.minScale);
        if (newScale !== scale.value) {
            const actualZoom = newScale / scale.value;
            scale.value = newScale;
            const elRect = workspaceContentEl.value!.getBoundingClientRect();
            moveTo(translate.x - (actualZoom - 1) * (origin.x - elRect.x), translate.y - (actualZoom - 1) * (origin.y - elRect.y));
            calcRect();
            emit('transform', {
                scale: scale.value,
                translate,
            });
        }
    });
}

function onWheel(event: WheelEvent) {
    if (event.ctrlKey) {
        const zoomIntensity = 0.2;
        const zoomValue = Math.exp(zoomIntensity * (event.deltaY < 0 ? 1 : -1));
        zoom({
            x: event.clientX,
            y: event.clientY,
        }, zoomValue);

        event.preventDefault();
        event.stopPropagation();
    }
}

const isScaling = ref(false);
const isDragging = ref(false);
watch(isDragging, (val) => {
    document.body.classList.toggle('dragging', val);
});

function onMiddleMouseDown() {
    if (props.dragOnMiddleMouse) {
        isDragging.value = true;
    }
}

function onMiddleMouseUp() {
    isDragging.value = false;
}

function onMouseDown(_event: MouseEvent) {
    if (props.dragOnLeftMouse) {
        isDragging.value = true;
    }
}

function onMouseUp() {
    isDragging.value = false;
}

function onDblClick(e: MouseEvent) {
    if (scale.value === 1) {
        const zoomIntensity = 1.3;
        const zoomValue = Math.exp(zoomIntensity);
        zoom({
            x: e.clientX,
            y: e.clientY,
        }, zoomValue);
    } else {
        reset();
    }
}

function moveTo(x: number, y: number) {
    if (props.bounds) {
        const maxXBound = (scale.value - 1) * rootRect.width * -1;
        const maxYBound = (scale.value - 1) * rootRect.height * -1;
        translate.x = Math.max(Math.min(0, x), maxXBound);
        translate.y = Math.max(Math.min(0, y), maxYBound);
    } else {
        translate.x = x;
        translate.y = y;
    }
    emit('transform', {
        scale: scale.value,
        translate,
    });
}

function onMouseMove(event: MouseEvent) {
    if (isDragging.value) {
        moveTo(translate.x + event.movementX, translate.y + event.movementY);
    }
}

let previousTouch: Touch | undefined;
let previousPinchDistance: number;

function onTouchStart(event: TouchEvent) {
    isDragging.value = event.touches.length === 1;
    isScaling.value = event.touches.length === 2;
    previousTouch = event.touches[0];
}

function onTouchEnd(event: TouchEvent) {
    isDragging.value = event.touches.length === 1;
    isScaling.value = event.touches.length === 2;
    previousTouch = event.touches[0];
}

function onTouchMove(event: TouchEvent) {
    if (props.dragOnlyZoomed && event.touches.length === 1 && scale.value === 1) {
        return;
    }

    event.preventDefault();

    if (isDragging.value && event.touches.length === 1) {
        const touch = event.touches[0];

        const movementX = touch.pageX - previousTouch!.pageX;
        const movementY = touch.pageY - previousTouch!.pageY;

        moveTo(translate.x + movementX, translate.y + movementY);

        previousTouch = touch;
    } else if (isScaling.value && event.touches.length === 2) {
        const pinchDistance = Math.sqrt(
            Math.pow(event.touches[1].clientX - event.touches[0].clientX, 2) +
            Math.pow(event.touches[1].clientY - event.touches[0].clientY, 2),
        );
        if (previousPinchDistance) {
            const origin = {
                x: event.touches[0].clientX + (event.touches[1].clientX - event.touches[0].clientX) * 0.5,
                y: event.touches[0].clientY + (event.touches[1].clientY - event.touches[0].clientY) * 0.5,
            };
            const zoomIntensity = 0.05;
            if (previousPinchDistance !== pinchDistance) {
                const zoomValue = 1 + zoomIntensity * (previousPinchDistance - pinchDistance < 0 ? 1 : -1);
                zoom(origin, zoomValue);
            }
        }
        previousPinchDistance = pinchDistance;
    }
}

function reset() {
    scale.value = 1;
    translate.x = translate.y = 0;
}

let rootRect: DOMRect;
let workspaceRect: DOMRect;

function center() {
    if (workspaceRect.width === 0 || workspaceRect.height === 0) {
        return;
    }

    const hScale = 0.9 * rootRect.height / workspaceRect.height;
    const wScale = 0.9 * rootRect.width / workspaceRect.width;
    const newScale = Math.min(hScale, wScale);
    scale.value = newScale;
    moveTo((rootRect.width - workspaceRect.width * newScale) * 0.5, (rootRect.height - workspaceRect.height * newScale) * 0.5);
}

function calcRect() {
    rootRect = rootEl.value!.getBoundingClientRect();
    workspaceRect = workspaceContentEl.value!.getBoundingClientRect();
    workspaceRect.width = workspaceRect.width * (1 / scale.value);
    workspaceRect.height = workspaceRect.height * (1 / scale.value);
}

onMounted(() => {
    calcRect();
    if (props.initialCenter) {
        center();
    }
});

defineExpose({
    reset,
    getScale() {
        return scale.value;
    },
    getTranslate() {
        return translate;
    },
    setScale(val: number) {
        scale.value = val;
    },
    setTranslate(val: {
        x: number,
        y: number
    }) {
        moveTo(val.x, val.y);
    },
    refresh: calcRect,
    center,
});
</script>
<style lang="scss" scoped>
//noinspection CssReplaceWithShorthandSafely
.zoom-content {
    position: relative;
    --background-grid-color: var(--blank-color);
    flex-grow: 1;
    overflow: hidden;
}

//noinspection CssReplaceWithShorthandSafely
.zoom-content--grid {
    background: linear-gradient(
            to right,
            var(--background-grid-color) 0,
            var(--background-grid-color) 1px,
            transparent 1px,
            transparent 100%
    ),
    linear-gradient(
            to bottom,
            var(--background-grid-color) 0,
            var(--background-grid-color) 1px,
            transparent 1px,
            transparent 100%
    );
    background-color: var(--easy-color);
    background-size: 10px 10px;
}

.zoom-content--grab {
    cursor: grab !important;
}

.zoom-content--grabbing {
    cursor: grabbing !important;
}

.zoom-content__content {
    position: relative;
    transform-origin: 0 0;
    width: max-content;
    max-width: 100%;
}
</style>
