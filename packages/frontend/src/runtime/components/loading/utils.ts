import { App, createApp, Directive, DirectiveBinding } from 'vue';
import LoadingComponent from './Loading.vue';

const INSTANCE_KEY = Symbol('CrLoading');
type LoadingEl = HTMLElement & {
    [INSTANCE_KEY]: {
        instance: App,
        instanceEl: HTMLElement,
        close: () => void
    }
};

function createInstance(el: LoadingEl, binding: DirectiveBinding<boolean>) {
    if (!el[INSTANCE_KEY]) {
        const instance = createApp(LoadingComponent, { overlay: true });
        const wrapper = document.createElement('div');
        instance.mount(wrapper);
        const instanceEl = wrapper.children[0] as HTMLElement;
        wrapper.removeChild(instanceEl);

        el.style.position = 'relative';

        el[INSTANCE_KEY] = {
            instance,
            instanceEl,
            close: () => {
                el.removeChild(instanceEl);
            },
        };
    }
    el.appendChild(el[INSTANCE_KEY].instanceEl);
}

export const vLoading: Directive<LoadingEl, boolean> = {
    mounted: (el, binding, vnode) => {
        if (binding.value) {
            createInstance(el, binding);
        }
    },
    updated(el, binding) {
        if (binding.value && !binding.oldValue) {
            createInstance(el, binding);
        } else if (!binding.value && binding.oldValue) {
            el[INSTANCE_KEY]?.close();
        }
    },
    unmounted(el) {
        el[INSTANCE_KEY]?.instance.unmount();
    },
};
