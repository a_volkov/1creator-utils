export function handleMouseLeave(element: HTMLElement | HTMLElement[], listener: () => void, delay = 300) {
    const elements = [element].flat();
    let timeLeftForLeave = delay;
    let timer: NodeJS.Timer;

    const mouseEnterListener = () => {
        clearInterval(timer);
        timeLeftForLeave = delay;
    };

    const tickTimer = () => {
        timeLeftForLeave -= 50;
        if (timeLeftForLeave <= 0) {
            elements.forEach(i => i.removeEventListener('mouseleave', mouseLeaveListener));
            elements.forEach(i => i.removeEventListener('mouseenter', mouseEnterListener));
            clearInterval(timer);
            listener();
        }
    };

    const mouseLeaveListener = () => {
        timer = setInterval(tickTimer, 50);
    };

    elements.forEach(i => i.addEventListener('mouseleave', mouseLeaveListener));
    elements.forEach(i => i.addEventListener('mouseenter', mouseEnterListener));
    return () => {
        clearInterval(timer);
        elements.forEach(i => i.removeEventListener('mouseleave', mouseLeaveListener));
        elements.forEach(i => i.removeEventListener('mouseenter', mouseEnterListener));
    };
}
