export function handleClickOutside(element: HTMLElement | HTMLElement[], listener: () => void) {
    const elements = [element].flat();
    const outsideClickListener = (event: MouseEvent) => {
        if (!elements.some(el => el.contains(event.target as HTMLElement))) {
            listener();
            document.removeEventListener('mousedown', outsideClickListener);
        }
    };

    document.addEventListener('mousedown', outsideClickListener);
    return () => {
        document.removeEventListener('mousedown', outsideClickListener);
    };
}
