export async function downloadFile(url: string, filename: string) {
    const response = await fetch(url, { method: 'GET' });
    const blob = await response.blob();
    const objUrl = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = objUrl;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    a.remove();
}
