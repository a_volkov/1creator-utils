import { vLoading } from './components/loading/utils';
import { defineNuxtPlugin } from '#imports';

export default defineNuxtPlugin((nuxtApp) => {
    nuxtApp.vueApp.directive('loading', vLoading);
});
