export default defineNuxtConfig({
  devtools: { enabled: false },

  app: {
      head: {
          viewport: 'width=device-width, initial-scale=1.0, user-scalable=no',
          htmlAttrs: { lang: 'ru' },
      },
  },

  css: ['~/app.scss'],
  imports: { autoImport: false },
  modules: ['../src/module'],
  compatibilityDate: '2025-01-04',
});