import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { AbstractHttpAdapter } from '@nestjs/core';
import { NotFoundError } from '@mikro-orm/core';

@Catch(NotFoundError)
export class EntityNotFoundExceptionFilter implements ExceptionFilter {
    constructor(private readonly httpAdapter: AbstractHttpAdapter) {
    }

    catch(exception: NotFoundError, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const responseBody = {
            statusCode: 404,
            message: exception.message,
        };

        this.httpAdapter.reply(
            ctx.getResponse(),
            responseBody,
            404,
        );
    }
}
