import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from '@nestjs/common';
import { AbstractHttpAdapter } from '@nestjs/core';
import { ApiError } from '@1creator/common';

@Catch(HttpException)
export class ApiExceptionFilter implements ExceptionFilter {
    constructor(private readonly httpAdapter: AbstractHttpAdapter) {
    }

    catch(exception: HttpException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();

        const nestResponse = exception.getResponse();

        const response: ApiError = typeof nestResponse === 'object'
            ? {
                statusCode: exception.getStatus(),
                name: exception.name,
                message: '',
                details: undefined,
                ...nestResponse,
            }
            : {
                statusCode: exception.getStatus(),
                message: nestResponse,
                details: undefined,
                name: exception.name,
            };

        this.httpAdapter.reply(
            ctx.getResponse(),
            response,
            exception.getStatus(),
        );
    }
}
