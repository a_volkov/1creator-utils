import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { AbstractHttpAdapter } from '@nestjs/core';
import { UniqueConstraintViolationException } from '@mikro-orm/core';

@Catch(UniqueConstraintViolationException)
export class UniqueConstraintViolationFilter implements ExceptionFilter {
    constructor(private readonly httpAdapter: AbstractHttpAdapter) {
    }

    localizeExceptionDetail(value: string) {
        return value
            .replace('Key', 'Значение')
            .replace('already exists', 'уже существует');
    }

    catch(exception: UniqueConstraintViolationException | { detail: string }, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const message =
      'detail' in exception
          ? this.localizeExceptionDetail(exception.detail as string)
          : exception.message;

        const responseBody = {
            statusCode: 404,
            message,
        };

        this.httpAdapter.reply(
            ctx.getResponse(),
            responseBody,
            400,
        );
    }
}
