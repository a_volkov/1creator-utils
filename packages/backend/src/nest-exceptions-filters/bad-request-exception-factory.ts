import { BadRequestException, ValidationError } from '@nestjs/common';
import { ApiErrorDetails } from '@1creator/common';

export function createValidationException(details: Record<string, string[]>) {
    return new BadRequestException({
        message: 'Неправильно заполнена форма',
        details,
    });
}

export function validationPipeExceptionFactory(opts: ValidationError[]) {
    const transformErrors = (e: ValidationError) => {
        if (e.children.length) {
            return e.children.reduce((acc, nestedE) => {
                acc[nestedE.property] = transformErrors(nestedE);
                return acc;
            }, {} as ApiErrorDetails<any>);
        } else {
            return Object.values(e.constraints);
        }
    };

    const details = opts.reduce((acc, e) => {
        acc[e.property] = transformErrors(e);
        return acc;
    }, {} as Record<string, any>);

    throw createValidationException(details);
}